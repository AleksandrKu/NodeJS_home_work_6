"use strict";
const express = require('express');
const app = express();
const routs = require('./routs');
const PORT = process.env.PORT || 9090;
const path = require('path');
const favicon = require('serve-favicon')

app.locals.messages = [{time_start: 'Time', username: 'User Name', message: 'Message', show: ''}];
app.locals.endAt = 0;
/*nunjucks*/
const nunjucks = require('nunjucks');
nunjucks.configure(path.join(__dirname, 'views'), {
    express: app
});

app.use(favicon(path.join(__dirname, 'favicon.ico')));
app.get('/', (req, res) => {
    res.render('index.twig', {title: "Express", messages: app.locals.messages});
});

app.use(express.urlencoded());
app.use(express.json());
app.use(routs);

app.listen(PORT, () => {
    console.log("Server port: ", PORT);
    // delete message
    let timerId = setInterval(function () {
        app.locals.messages.forEach(function (item, i, arr) {
            if (item.endAt && (+item.endAt < +Math.ceil(Date.now() / 1000))) {
                delete arr[i];
            }
        });
    }, 2000);

});