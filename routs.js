const express = require('express');
const app = express();

const {Router} = require('express');
const router = Router();

const path = require('path');
const fileType = require('file-type');
const fs = require('fs');
const middlewares = require('./middlewares');


const nunjucks = require('nunjucks');
nunjucks.configure(path.join(__dirname, 'views'), {
    express: app
});


function function_time_seconds(time) {
    return time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + ":" + time.getMilliseconds();
}


const not_found = function (res) {
    res.writeHead(404);
    res.end("Not found");
};


router.get("/get-images", (req, res) => {
    fs.readdir(path.join(__dirname, 'images'), (err, files) => {
        console.log(err);
        console.log(files[1]);
        res.json({data: files});
    });
});

router.get("/get-messages", (req, res) => {
        res.json({data: req.app.locals});
});

router.get("/show-messages", (req, res) => {
    res.render('messages.twig', {title: "Express", messages: req.app.locals.messages});
    console.log(req.app.locals.messages);
});


router.post("/messages",   middlewares.validateMessage, (req, res) => {
    let obj = req.body;
    obj.time_start = function_time_seconds(new Date());
    req.app.locals.messages.push(obj);
    res.send(JSON.stringify({message: "Message added successful", data: obj }));
    res.end();
});


router.get('/images/:image', middlewares.checkImage ,  (req, res) => {
    const image_path = path.join(__dirname, "images", req.params.image);
    const read_stream = fs.createReadStream(image_path);
    read_stream.on('open', () => {
        read_stream.once('data', (chunk) => {
          let  file_type = fileType(chunk);
            if (file_type) {
                res.setHeader("Content-Type", file_type.mime);
            }
        });
        read_stream.pipe(res);
    });

});

router.get('/*', function (req, res) {
    not_found(res);
    res.end();
});

module.exports = router;