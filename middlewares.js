"use strict";
const path = require('path');
const fs = require('fs');

exports.validateMessage = (req, res, next) => {
    //  "username": строка, минимум 3 символа, только латиница,
    //  "message": строка, минимум 1, максимум 512 символов,
    //  "show": целое число, минимум 1, максимум 60, количество секунд, сколько будет отображаться сообщение.
    console.log(typeof req.body.username);

    let error = false;
    const empty = (name) => {
        res.status(404).json({message: "Not found property " + name});
        error = true;
    };

    if (req.body.username) {
        if (typeof req.body.username != "string") {
            res.status(406).json({message: req.body.username + "No string"});
            error = true;
        } else if (req.body.username.length < 3) {
            res.status(406).json({message: req.body.username + "Less 3 characters"});
            error = true;
        } else if (/^[a-zA-Z]+$/.test(req.body.username) === false) {
            res.status(406).json({message: req.body.username + " Only Latin characters"});
            error = true;
        }
    } else {
        empty('username');
    }


    if (req.body.message) {
        if (req.body.message.length < 3 || req.body.message.length > 512) {
            res.status(406).json({message: " Message must be from 3 to 512 characters"});
            error = true;
        }
    } else {
        empty('message')
    }


    if (req.body.show) {
        if (!Number.isInteger(+req.body.show)) {
            res.status(418).json({message: "Number is not a integer"});
            error = true;
        } else if (req.body.show < 1 || req.body.show > 60) {
            res.status(406).json({message: " Number must be from 1 to 60 characters"});
            error = true;
        }
    } else {
        empty('show');
    }

    if (!error) {
        req.body.endAt =  +Math.ceil(Date.now()/1000) + (+req.body.show);
        req.app.locals.endAt = req.body.endAt;
        console.log(req.app.locals.endAt);
        next();
    }
};

exports.checkImage = ( req, res, next ) => {

        const image_path = path.join(__dirname, "images", req.params.image);
        fs.stat(image_path, (err, stats) => {
            if (!(err == null && (stats["mode"] & 4))) { // file is readable
                const error = (err.code == "ENOENT") ? "No such file" : "Error";
                console.log(error);
                res.writeHead(404);
                res.end("Not found");
                next(new Error('Error'));
            } else { next(); }
        });

};